@extends('layouts.app')
@section('content')
    <div class="container justify-content-center">
        <a href="{{route('tasks.create')}}" class="btn btn-outline-primary btn-sm text-bolder mb-3">ADD TASK</a>
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Name</th>
                <th scope="col">Content</th>
                <th scope="col" colspan="3">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tasks as $task)
                <tr>
                    <td>
                        {{$task->id}}
                    </td>
                    <td>
                        {{$task->name}}
                    </td>
                    <td>
                        {{$task->content}}
                    </td>
                    <td>
                        <a href="{{route('tasks.show', $task->id)}}" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                    </td>
                    <td>
                        <a href="{{route('tasks.edit', $task->id)}}" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></a>
                    </td>
                    <td>
                        <form action="{{route('tasks.destroy', $task->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{$tasks->links()}}
    </div>
@endsection
