@extends('layouts.app')

@section('content')
    <div class="container justify-content-center">
        <div class="row">
            <div class="col col-md-6 offset-md-3">
                <div class="card mt-5">
                    <div class="card-header">
                        <h2 class="text-center text-bold">EDIT TASK</h2>
                    </div>

                    <div class="card-body">
                        <form action="{{route('tasks.update', $task->id)}}" method="post">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <input class="form-control mb-2" name="name" value="{{$task->name}}" placeholder="Name ...">
                                @error('name')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <textarea class="form-control mb-2" name="content" rows="4" placeholder="Content ...">{{$task->content}}</textarea>
                                @error('content')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <input class="btn btn-success btn-block" type="submit" value="SAVE">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
