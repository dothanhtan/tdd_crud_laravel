<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class EditTaskTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_edit_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->put($this->getEditTaskRoute($task->id), $task->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('tasks', $task->toArray());
        $response->assertRedirect(route('tasks.index'));
    }

    /** @test */
    public function unauthenticated_user_can_not_edit_task()
    {
        $task = Task::factory()->create();
        $response = $this->put($this->getEditTaskRoute($task->id), $task->toArray());
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_can_not_edit_task_if_name_is_null()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $data = [
            'name' => '',
            'content' => $task->content
        ];
        $response = $this->put($this->getEditTaskRoute($task->id), $data);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_user_can_not_edit_task_if_content_is_null()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $data = [
            'name'      => $task->name,
            'content'   => ''
        ];
        $response = $this->put($this->getEditTaskRoute($task->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['content']);
    }

    /** @test */
    public function authenticated_user_can_not_edit_task_if_data_is_null()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $data = [
            'name'      => '',
            'content'   => ''
        ];
        $response = $this->put($this->getEditTaskRoute($task->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name', 'content']);
    }

    /** @test */
    public function authenticated_user_can_see_edit_task_form()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->get($this->getEditTaskViewRoute($task->id));
        $response->assertViewIs('tasks.edit');
    }

    /** @test */
    public function authenticated_user_can_see_edit_task_form_if_validate_error()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $data = [
            'name' => '',
            'content' => ''
        ];
        $response = $this->from($this->getEditTaskViewRoute($task->id))->put($this->getEditTaskRoute($task->id), $data);
        $response->assertRedirect($this->getEditTaskViewRoute($task->id));
    }

    /** @test */
    public function unauthenticated_user_can_not_see_edit_task_form()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getEditTaskViewRoute($task->id));
        $response->assertRedirect('/login');
    }

    public function getEditTaskRoute($id)
    {
        return route('tasks.update', $id);
    }

    public function getEditTaskViewRoute($id)
    {
        return route('tasks.edit', $id);
    }
}
