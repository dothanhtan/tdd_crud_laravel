<?php

namespace Tests\Feature;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetListTaskTest extends TestCase
{
    /** @test */
    public function user_can_get_all_test()
    {
        $task = Task::factory()->create();
        $response = $this->get('/tasks');

        $response->assertStatus(200);
        $response->assertViewIs('tasks.index');
    }
}
