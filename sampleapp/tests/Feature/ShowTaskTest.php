<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ShowTaskTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_show_task_detail()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->get($this->getShowTaskRoute($task->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.show');
    }

    /** @test */
    public function unauthenticated_user_can_not_show_task_detail()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getShowTaskRoute($task->id));
        $response->assertRedirect('/login');
    }

    public function getShowTaskRoute($id)
    {
        return route('tasks.show', $id);
    }
}
